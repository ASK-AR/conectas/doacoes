# Projeto Doacoes

Divulgação da pesquisa de doadores de alta renda relizada pela
FGV EAESP - Centro de Estudos em Administração Pública e Governo,
FGV EAESP - GVpesquisa e Conectas Direitos Humanos, com financiamento do
Fundo Bis.

Este projeto foi desenvolvido produzido pela [ASK-AR](https://ask-ar.xyz).

Este é um site estático, produzido com o tema base
[Hyperspace](https://html5up.net/hyperspace) e que se utiliza do plugin
[pivottable.js](http://pivottable.js.org/).

O projeto conta também com um Makefile com algumas opções que ajudam no
desenvolvimento. Utiliza `make help` para ver algumas das opções disponíveis.

Por fim, o site conta também com um pipeline de CI/CD no gitlab para integração
contínua.
