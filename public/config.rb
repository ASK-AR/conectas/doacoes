http_path = "/"
css_dir = "assets/css"
images_dir = "assets/images"
fonts_dir = "assets/fonts"
sass_dir = "sass"

output_style = (environment == :production) ? :compressed : :expanded
sourcemap = true

preferred_syntax = :scss
