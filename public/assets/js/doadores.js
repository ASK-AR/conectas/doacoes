window._global_counter = 0;
window._rendered_counter = 0;
window._bool_all_rendered = false;
window._tooltip_already_rendered = false;

function count_rendered_bases(){
  window._rendered_counter += 1;
  if (window._rendered_counter == window._global_counter && window._bool_all_rendered && window._tooltip_already_rendered == false){
    window._tooltip_already_rendered = true;
    $('.data-table').each((idx,obj)=>{enable_tooltips(obj.id)});
  }
}

function count_bases() {
  window._global_counter += 1;
}

function enable_tooltips(table_id){
  $("#"+table_id+" .pvtAttr").tooltip({
    position: {
      my: "left center",
      at: "right+5 center",
      collision: "none",
    },
    hide: false,
    show: false,
    // disabled: $("#"+table_id).css("display") == 'none',
    classes: {
      "ui-tooltip": "doadores-tooltip tooltip-right"
    },
    close: (a, b) => {
      $(".ui-helper-hidden-accessible").each((idx, el) => {
        $(el).find("div:not(:last)").remove();
      })
    },
  });
}

function _build_pivottable(target, data_file, tooltips_file) {

  function _build_pivotttable_internal(tooltips=undefined){
    Papa.parse(data_file, {
      download: true,
      skipEmptyLines: true,
      complete: function(parsed) {
        $("#" + target).pivotUI(
          parsed.data,
          {
            menuLimit: 100,
            onRefresh: (config) => {
              exporters = ["TSV Export", "CSV Export"]
              if (exporters.indexOf(config.rendererName) >= 0) {
                $("#download_data_container").show();
              } else {
                $("#download_data_container").hide();
              }
              $(".pvtAttr").tooltip("close");
            },
            renderers: $.extend(
              $.pivotUtilities.renderers,
              $.pivotUtilities.c3_renderers,
              $.pivotUtilities.d3_renderers,
              $.pivotUtilities.export_renderers
            ),
            rendererName: "Table",
            tooltips: tooltips
          },
          null,
          "pt"
        );
        count_rendered_bases();
      }
    })
  }

  Papa.parse(tooltips_file, {
    download: true,
    skipEmptyLines: true,
    header: true,
    complete: function(parsed) {
      _build_pivotttable_internal(parsed.data[0]);
    },
    error: (err, file, inputElem, reason) => {
      _build_pivotttable_internal();
    }
  })
}

function build_pivottables() {
  let bases = {
    doadores1: {
      data_file: "/data/labels_habitos.csv",
      tooltips_file: "/data/labels_habitos_descricoes.csv"
    },
    doadores2: {
      data_file: "/data/labels_dh.csv",
      tooltips_file: "/data/labels_dh_descricoes.csv"
    }
  }

  for (let target in bases) {
    count_bases();
    let data_file = bases[target].data_file,
        tooltips_file = bases[target].tooltips_file;
    _build_pivottable(target, data_file, tooltips_file);
  }

  window._bool_all_rendered = true;
}

$(document).ready(function(){
  function download_csv(e){
    e.preventDefault();
    content = $(".data-table:visible textarea")[0].textContent;
    saveTextAs(
      content,
      `${$("input[name='select-subset']:checked").attr("id")}.csv`
    )
  }

  function toggle_tables(){
    $(".data-table").each((idx, obj) => {
      $(obj).toggle();
      $("#" + obj.id + " .pvtAttr").tooltip(
        "option", "disabled", $(obj).css("display") == "none"
      )
    })
  }

  $("#download_data").on("click", download_csv);
  $("#download_data_container").hide();
  $("input[type=radio][name='select-subset']").change(toggle_tables);
  build_pivottables();
$})
