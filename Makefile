SASS_DIR := public/assets/sass
CSS_DIR := public/assets/css
ifndef INTERNAL_PORT
	INTERNAL_PORT := 8000
endif
ifndef PUBLIC_PORT
	PUBLIC_PORT := 8000
endif
RUBY_RUN_HTTP := ruby -run -e httpd public -p ${INTERNAL_PORT}
PYTHON_RUN_HTTP := python -m http.server -d public

ifndef DOCKER_IMAGE
	DOCKER_IMAGE := askbr/compass:latest
endif

ifndef ENTRYPOINT
	ENTRYPOINT := sh -c "compass watch --poll public & ruby -run -e httpd public -p ${INTERNAL_PORT}"
endif

DOCKER_BASE_CMD := docker run --rm -it -v $(shell pwd):/project --name conectas_doacoes

PIVOTTABLE_DIST_DIR := dependencies/pivottable/dist
JS_ASSETS_DIR := public/assets/js/

PIVOTTABLE_FILES := ${PIVOTTABLE_DIST_DIR}/pivot.min.js
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/pivot.min.js.map
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/pivot.pt.min.js
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/pivot.pt.min.js.map
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/export_renderers.min.js
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/export_renderers.min.js.map
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/c3_renderers.min.js
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/c3_renderers.min.js.map
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/d3_renderers.min.js
PIVOTTABLE_FILES += ${PIVOTTABLE_DIST_DIR}/d3_renderers.min.js.map

.DEFAULT_GOAL : help

help:
	@echo "Welcome to Carcerópolis make file."
	@echo "This file is intended to ease your life regarding docker-compose commands."
	@echo "Bellow you will find the options you have with this makefile."
	@echo "You just need to run 'make <command> <arguments..>'."
	@echo "    "
	@echo "    help - Print this help message"
	@echo "    "
	@echo "    compile_css"
	@echo "        Generate the CSS file from SASS files."
	@echo "    watch"
	@echo "        Start the container and run compass in watch mode, in foreground."
	@echo "        It will rebuild the css file after every change in sass files."
	@echo "    watch_bg"
	@echo "        Start the container and run compass in watch mode, in background."
	@echo "        It will rebuild the css file after every change in sass files."
	@echo "        "
	@echo "    run"
	@echo "        Start the project container, run the compass compiler in "
	@echo "        watch mode and in background, and start a ruby webserver,"
	@echo "        FOR DEVELOPMENT PURPOSES ONLY."
.PHONY: help

compile_css:
	${DOCKER_BASE_CMD} ${DOCKER_IMAGE} compass compile public
.PHONY: css

watch:
	compass watch --poll public
.PHONY: watch

watch_bg:
	compass watch --poll public &
.PHONY: watch_bg

run:
	${DOCKER_BASE_CMD} -p ${PUBLIC_PORT}:${INTERNAL_PORT} ${DOCKER_IMAGE} ${ENTRYPOINT}
.PHONY: run

local_static_css_gen:
	gem install compass && compass compile public
.PHONY: local_static_css_gen

pivottable_setup:
	cd dependencies/pivottable; npm install

pivottable_build: pivottable_setup
	cd dependencies/pivottable; node_modules/gulp/bin/gulp.js

pivottable_watch: pivottable_setup
	cd dependencies/pivottable; node_modules/gulp/bin/gulp.js watch serve
.PHONY: pivottable_watch

update_pivottable_lib: pivottable_build
	cp --remove-destination ${PIVOTTABLE_FILES} ${JS_ASSETS_DIR}
.PHONY: unlink_pivottable_lib
