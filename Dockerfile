# Image with compass and Ruby
FROM ubuntu:18.10
MAINTAINER Diego Rabatone Oliveira <diraol (at) ask-ar (dot) xyz>

WORKDIR /project

RUN set -ex \
    && apt-get update \
    && apt install -y ruby-dev make ruby-ffi \
    && gem install --no-rdoc --no-ri compass
